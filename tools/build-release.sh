#!/usr/bin/env bash

nvm use 16.13

cp config.xml config_.xml
sed -i 's/de.echoapp.debug/de.echoapp/' config.xml
sed -i 's/Echo Debug/Echo/' config.xml

cordova platform remove android
cordova platform add android

export GOOGLE_MAPS_API_KEY=AIzaSyCVzdiXCm6e-qGVjl_a2-As3Y6z9vyJt_k
export MODE=production
export HOST_URI=https://echoapp.de
export CONTACT_EMAIL=kontakt@echoapp.de
export MATOMO_ID=1

npm run "cordova browser prepare"
rm -rf src/server/public
mv platforms/browser/www src/server/public

echo "copying signing..."
cp tools/signing/echo.jks platforms/android/app/
cp tools/signing/release-signing.properties platforms/android/app/

export JAVA_HOME=/usr/lib/jvm/java-8-openjdk;
export PATH=$JAVA_HOME/bin:$PATH;
export ORG_GRADLE_PROJECT_cdvReleaseSigningPropertiesFile=release-signing.properties

npm run "cordova release android"

mv platforms/android/app/build/outputs/apk/release/app-release.apk src/server/public/echo.apk

rm config.xml
mv config_.xml config.xml
cordova platform remove android
cordova platform add android

git add src/server/public/ -v
